resource "google_container_cluster" "gke_private" {
  count = var.cluster_configuration != {} ? 1 : 0

  project    = var.project_id
  location   = var.region
  network    = "https://www.googleapis.com/compute/v1/projects/${var.host_project_name}/global/networks/${var.network_name}"
  subnetwork = "projects/${var.host_project_name}/regions/${var.region}/subnetworks/${var.subnetwork_name}"

  min_master_version = lookup(var.cluster_configuration, "gke_version")

  networking_mode = lookup(var.cluster_configuration, "networking_mode")

  logging_service       = lookup(var.cluster_configuration, "logging_service") ? "logging.googleapis.com/kubernetes" : "none"
  monitoring_service    = lookup(var.cluster_configuration, "monitoring_service") ? "monitoring.googleapis.com/kubernetes" : "none"
  enable_shielded_nodes = lookup(var.cluster_configuration, "shielded_nodes", false)

  name        = "${var.prefix_name}-cluster-${var.environment}"
  description = "${upper(var.prefix_name)} Cluster for ${upper(var.environment)} Environment"

  remove_default_node_pool  = lookup(var.cluster_configuration, "remove_node_pool", true)
  initial_node_count        = lookup(var.cluster_configuration, "initial_node_count", 1)
  default_max_pods_per_node = lookup(var.cluster_configuration, "max_pods_per_node")

  vertical_pod_autoscaling {
    enabled = lookup(var.cluster_configuration, "vertical_pod_autoscaling", false)
  }

  private_cluster_config {
    master_ipv4_cidr_block  = lookup(var.cluster_configuration, "master_cidr_block")
    enable_private_nodes    = lookup(var.cluster_configuration, "private_nodes")
    enable_private_endpoint = lookup(var.cluster_configuration, "private_endpoint")
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = var.subnet_pods_name     # * The name of the secondary range within the subnetwork for the cluster to use *
    services_secondary_range_name = var.subnet_services_name # * The name of the secondary range within the subnetwork for the services to use *
  }

  master_auth {
    client_certificate_config {
      issue_client_certificate = "false"
    }
  }

  master_authorized_networks_config {
    dynamic "cidr_blocks" {
      for_each = lookup(var.ssh_bastion, "create_local_bastion") == true ? [""] : []
      content {
        display_name = "${var.prefix_name}-bastion-ssh-${var.environment}"
        cidr_block   = "${var.bastion_address}/32"
      }
    }
    dynamic "cidr_blocks" {
      for_each = var.allowed_ips_to_master
      content {
        display_name = cidr_blocks.value.display_name
        cidr_block   = cidr_blocks.value.cidr_block
      }
    }
  }

  node_config {
    tags = ["private"]
  }

  maintenance_policy {
    recurring_window {
      start_time = "2019-01-01T01:00:00Z"
      end_time   = "2019-01-01T05:00:00Z"
      recurrence = "FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR"
    }
  }

  lifecycle {
    ignore_changes = [
      node_config,
    ]
  }
}


resource "google_container_node_pool" "node_pool" {
  for_each = {for node_pool in var.node_pools : node_pool.node_pool_name => node_pool}

  project  = var.project_id
  location = var.region

  cluster            = google_container_cluster.gke_private[0].name
  initial_node_count = lookup(each.value, "initial_node_count", "1")
  node_locations     = lookup(each.value, "node_per_zone") == true ? sort(var.available_zones) : [element(sort(var.available_zones), 0)]

  name = "${lookup(each.value, "node_pool_name")}-node-pool-${var.environment}"

  autoscaling {
    min_node_count = lookup(each.value, "min_node_count", "1")
    max_node_count = lookup(each.value, "max_node_count", "1")
  }

  management {
    auto_repair  = lookup(each.value, "auto_repair", "true")
    auto_upgrade = lookup(each.value, "auto_upgrade", "true")
  }

  node_config {
    machine_type = lookup(each.value, "instance_type", "e2-micro")

    disk_size_gb = lookup(each.value, "disk_size", "30")
    disk_type    = "pd-standard"
    preemptible  = false

    tags = ["private"]

    labels = lookup(each.value, "labels")[0]

    taint = lookup(each.value, "taint", [])
    
    oauth_scopes = lookup(var.cluster_configuration, "monitoring_service") ? [
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/monitoring.write"
    ] : ["https://www.googleapis.com/auth/monitoring.write"]
  }
}

